package myapp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class CalendarEventsServiceSpec extends Specification {

    CalendarEventsService calendarEventsService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new CalendarEvents(...).save(flush: true, failOnError: true)
        //new CalendarEvents(...).save(flush: true, failOnError: true)
        //CalendarEvents calendarEvents = new CalendarEvents(...).save(flush: true, failOnError: true)
        //new CalendarEvents(...).save(flush: true, failOnError: true)
        //new CalendarEvents(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //calendarEvents.id
    }

    void "test get"() {
        setupData()

        expect:
        calendarEventsService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<CalendarEvents> calendarEventsList = calendarEventsService.list(max: 2, offset: 2)

        then:
        calendarEventsList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        calendarEventsService.count() == 5
    }

    void "test delete"() {
        Long calendarEventsId = setupData()

        expect:
        calendarEventsService.count() == 5

        when:
        calendarEventsService.delete(calendarEventsId)
        sessionFactory.currentSession.flush()

        then:
        calendarEventsService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        CalendarEvents calendarEvents = new CalendarEvents()
        calendarEventsService.save(calendarEvents)

        then:
        calendarEvents.id != null
    }
}
