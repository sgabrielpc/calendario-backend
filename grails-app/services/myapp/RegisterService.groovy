package myapp

import grails.gorm.transactions.Transactional
import org.grails.web.json.JSONObject

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Transactional
class RegisterService {

    JSONObject registerNewUser(HttpServletRequest request, HttpServletResponse response) {
        JSONObject jsonObject = request.JSON

        if (checkUserExists(request,response).message == true) {
            return new JSONObject(message: "User already exists");
        }

        def userRole = new Role(authority: 'ROLE_USER').save()
        def me = new User(username: jsonObject.username, password: jsonObject.password, firstName: jsonObject.firstName, lastName: jsonObject.lastName).save()

        UserRole.create me, userRole

        UserRole.withSession {
            it.flush()
            it.clear()
        }

        if (checkUserExists(request,response).message == false) {
            return new JSONObject(message: "Failed to create user");
        }


        return  new JSONObject(message: 'OK')
    }

    JSONObject checkUserExists(HttpServletRequest request, HttpServletResponse response) {
        JSONObject jsonObject = request.JSON

        if (User.findByUsername(jsonObject.username) == null) {
            return new JSONObject(message: false)
        }

        return new JSONObject(message: true)
    }
}


