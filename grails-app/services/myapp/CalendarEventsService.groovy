package myapp

import grails.gorm.services.Service

@Service(CalendarEvents)
interface CalendarEventsService {

    CalendarEvents get(Serializable id)

    List<CalendarEvents> list(Map args)

    Long count()

    void delete(Serializable id)

    CalendarEvents save(CalendarEvents calendarEvents)

}