package myapp

import grails.gorm.transactions.Transactional
import org.grails.web.json.JSONObject

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Transactional
class EventsService {

    def printEvents() {
        println "\n\nLista de Eventos:\n"
        CalendarEvents.each {
            println ("\t" + it);
        }
    }

    def springSecurityService

    CalendarEvents[] getEvents() {

        def principal = springSecurityService.principal
        println User.findByUsername(principal.username)

        CalendarEvents[] events = CalendarEvents.findAllByUser(User.findByUsername(principal.username))

        if (events == null) {
            return []
        } else {
            return events
        }
    }



    JSONObject addEvents(HttpServletRequest request, HttpServletResponse response) {
        JSONObject[] events = request.JSON;

        def errors = false;
        String message = "OK"

        def principal = springSecurityService.principal
        def loggedInUser = User.findByUsername(principal.username)

        events.each {
            println "\nAdd event: " + it.title

            CalendarEvents eventAdded = new CalendarEvents(
                    title: it.title,
                    start: new Date(it.start),
                    end: new Date(it.end),
                    primaryColor: it.color.primary,
                    secondaryColor: it.color.secondary,
                    allDay: false,
                    resizable: true,
                    draggable: true,
                    user: loggedInUser)
                    .save()

            if (eventAdded == null) {
                println "Failed to add event " + it.title
                errors = true;
                message = "Failed to add event"
            }
        }

        return new JSONObject("message" : message)
    }

    JSONObject updateEvents(HttpServletRequest request, HttpServletResponse response) {
        JSONObject[] events = request.JSON;

        boolean errors = false
        String message = "OK"

        print "\nEvents o update: "
        println events

        println "\n\nUpdate events:"
        events.each {
            CalendarEvents eventToUpdate = CalendarEvents.get(it.id)

            if (eventToUpdate == null) {
                println "Could not find event with id: " + it.id
                errors = true;
                message = "Failed to update one or more events (Code: 01)"
            }

            eventToUpdate.title = it.title;
            eventToUpdate.start = new Date(it.start);

            if (it.end != null) {
                eventToUpdate.end = new Date(it.end);
            }

            eventToUpdate.primaryColor = it.color.primary
            eventToUpdate.secondaryColor = it.color.secondary

            if (eventToUpdate.save() == null) {
                printf "Failed to update event: " + eventToUpdate.title
                errors = true
                message = "Failed to update one or more events (Code: 02)"
            } else {
                println "Updated: " + it.title
            }
        }

        printEvents()

        return new JSONObject("message" : message)

    }
}
