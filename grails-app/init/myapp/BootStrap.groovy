package myapp




class BootStrap {


    def init = { servletContext ->

        println "Hello World!"

        def userRole = new Role(authority: 'ROLE_ADMIN').save()
        def me = new User(username: "admin", password: "admin", firstName: "Admin", lastName: "Admin").save()

        UserRole.create me, userRole

        UserRole.withSession {
            it.flush()
            it.clear()
        }


        int todayDay = 14;
        int todayMonth = 1;
        int todayYear  = 2018;



        new CalendarEvents(  title: "A 3 day event",
                start: new Date(todayYear - 1900, todayMonth, todayDay- 1, 13, 30, 0),
                end: new Date(todayYear - 1900, todayMonth, todayDay + 1, 0, 0, 0),
                primaryColor: "#ad2121",
                secondaryColor: "#FAE3E3",
                allDay: false,
                resizable: true,
                draggable: true,
                user: User.findByUsername("admin")).save();

        new CalendarEvents(title: "An event with no end date",
                 start: new Date(todayYear - 1900, todayMonth , todayDay - 5, 0, 0, 0),
                 end: new Date(todayYear - 1900, todayMonth , todayDay - 5, 23, 59, 0),
                 primaryColor: "#e3bc08",
                 secondaryColor: "#FDF1BA",
                 allDay: true,
                 resizable: true,
                 draggable: true,
                 user: User.findByUsername("admin")).save();

         new CalendarEvents(title: "A long event that spans 2 months",
                 start: new Date(todayYear - 1900, todayMonth, 25, 23, 59, 0),
                 end: new Date(todayYear - 1900, todayMonth + 1 , 3, 23, 59, 0),
                 primaryColor: "#1e90ff",
                 secondaryColor: "#D1E8FF",
                 allDay: false,
                 resizable: true,
                 draggable: true,
                 user: User.findByUsername("admin")).save();


         new CalendarEvents(title: "A draggable and resizable event",
                 start: new Date(todayYear - 1900, todayMonth,todayDay + 5, 10, 30, 0),
                 end: new Date(todayYear - 1900, todayMonth,todayDay + 5, 20, 00, 0),
                 primaryColor: "#e3bc08",
                 secondaryColor: "#FDF1BA",
                 allDay: false,
                 resizable: true,
                 draggable: true,
                 user: User.findByUsername("admin")).save();

        new CalendarEvents(title: "A Conflicted event",
                start: new Date(todayYear - 1900, todayMonth,todayDay, 10, 31, 0),
                end: new Date(todayYear - 1900, todayMonth,todayDay, 19, 00, 0),
                primaryColor: "#ff69b4",
                secondaryColor: "#FDF1BA",
                allDay: false,
                resizable: true,
                draggable: true,
                user: User.findByUsername("admin")).save();

    }
    def destroy = {
    }
}
