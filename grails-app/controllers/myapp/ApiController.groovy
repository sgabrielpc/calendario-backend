package myapp

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.grails.web.json.JSONObject

class ApiController {

    def eventsService
    def registerService

    @Secured(['ROLE_USER'])
    def announcements() {
        render (contentType: "application/json") {
            evento {
                for (a in calendarEventsService.lastEvents()) {
                    evento(message: a.message)
                }
            }
        }
    }

    @Secured(['permitAll'])
    def register() {
        render registerService.registerNewUser(request, response)
    }

    @Secured(['permitAll'])
    def userExists() {
        render registerService.checkUserExists(request, response)
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def getEvents() {
        render eventsService.getEvents() as JSON
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def addEvents() {
        render eventsService.addEvents(request, response)
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def updateEvents() {
        render eventsService.updateEvents(request, response)
    }
}
