package myapp

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class CalendarEventsController {

    CalendarEventsService calendarEventsService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond calendarEventsService.list(params), model:[calendarEventsCount: calendarEventsService.count()]
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def show(Long id) {
        respond calendarEventsService.get(id)
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def create() {
        respond new CalendarEvents(params)
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def save(CalendarEvents calendarEvents) {
        if (calendarEvents == null) {
            notFound()
            return
        }

        try {
            calendarEventsService.save(calendarEvents)
        } catch (ValidationException e) {
            respond calendarEvents.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'calendarEvents.label', default: 'CalendarEvents'), calendarEvents.id])
                redirect calendarEvents
            }
            '*' { respond calendarEvents, [status: CREATED] }
        }
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def edit(Long id) {
        respond calendarEventsService.get(id)
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def update(CalendarEvents calendarEvents) {
        if (calendarEvents == null) {
            notFound()
            return
        }

        try {
            calendarEventsService.save(calendarEvents)
        } catch (ValidationException e) {
            respond calendarEvents.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'calendarEvents.label', default: 'CalendarEvents'), calendarEvents.id])
                redirect calendarEvents
            }
            '*'{ respond calendarEvents, [status: OK] }
        }
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        calendarEventsService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'calendarEvents.label', default: 'CalendarEvents'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    @Secured(["ROLE_ADMIN", "ROLE_USER"])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'calendarEvents.label', default: 'CalendarEvents'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
