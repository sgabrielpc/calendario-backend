package myapp

class CalendarEvents {
    String title
    Date start
    Date end
    String primaryColor
    String secondaryColor
    boolean allDay
    boolean resizable
    boolean draggable
    User user;

    static constraints = {
        title nullable: false, blank: false
        start nullable: false, blank: false
        end nullable: true, blank: true
        user nullable: false, blank: false
    }
}
